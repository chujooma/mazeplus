# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This is a testbed for mazeplus.
Mazeplus is a rewrite of an old program that generates a random maze.

* Version

2.0 2018-07-24 GPLv3

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Extract all files into one directory. Run make.
This creates an executable called mazeplus.

* Configuration
* Dependencies

Mazeplus currently only generates Postscript code.
You can use ps2pdf to produce a PDF file from the result.

* Database configuration
* How to run tests

Some files have local tests. To run them, type:

`g++ -DLOCALTEST file.cpp`

followed by

`./a.out`

This will run all tests available for the file. Local tests work for:
mazeargs.cpp, mazefreelist.cpp, mazegrid.cpp

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: F Lundevall
* Other community or team contact