/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _MAZEGRID_
#define _MAZEGRID_

enum GridPointProperties { OffGrid = -1, IsPath = 0, IsWall = 1 };

/* Default sizes, used by the default constructor.
   The default constructor is typically used for grids
   that are later overwritten by the copy constructor 
   (or by the assignment constructor). So we make these
   default grids ridiculously small, so they only use
   a minimal amount of resources before being
   deleted and replaced. */
const int GridXDefaultSize = 3; // Smallest possible grid
const int GridYDefaultSize = 3;

/*
 * Class Mazegrid implements a grid for a maze.
 * The class makes sure that the grid has a reasonable size.
 * Member function getsize returns the size.
 * A freshly constructed grid is initialized as empty,
 * with no walls anywhere.
 * You can set or clear wall points with setpoint.
 * You can check a point with iswallpoint.
 * There are shortcuts addwallpoint and clearwallpoint
 * for convenience.
 * All the xxxpoint functions return the old contents
 * of the grid point, coded as follows:
 * 0 = no wall, 1 = wall, -1 = position is off grid
 */

class Mazegrid
{
 public:
  Mazegrid( void ); // Default constructor  
  Mazegrid( const int x, const int y ); // Constructor with size arguments
  Mazegrid( const Mazegrid& originalmaze ); // Copy constructor
  Mazegrid& operator =( const Mazegrid& rightsidemaze ); // Assignment
  ~Mazegrid( void ); // Destructor
std::pair<int,int> getsize( void ) const;
  int addwallpoint( const int x, const int y );
  int clearwallpoint( const int x, const int y );
  int iswallpoint( const int x, const int y ) const; // returns value, -1 if off grid
 private:
  char * mazepicture;
  int xsize;
  int ysize;
  void init( const int x, const int y );
  int setpoint( const int x, const int y , const GridPointProperties wall ); // returns old val, -1 if off grid
};
#endif
