CC = g++
CFLAGS = -O -std=c++11
LDFLAGS =
OBJS = $(patsubst %.cpp,%.o,$(wildcard *.cpp))
DEPS = *.hpp

%.o: %.cpp $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

mazeplus: $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -f *.o *~

debug:  CFLAGS = -g -DDEBUG
debug:	mazeplus 
