/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <cassert>
#include <iostream>
#include <string>
#include <utility>

#include "main.hpp"
#include "mazegrid.hpp"
#include "mazepsprint.hpp"

using namespace std;

string MazePSconverter::maze2string( const Mazegrid maze )
{
  int xlim = maze.getsize().first;
  int ylim = maze.getsize().second;
  float xscale = 5*55.0/xlim;
  float yscale = 5*39.0/ylim;
  output =  "%!\n";
  output += "%%BoundingBox: 0 0 " +
    std::to_string( PostScriptPageSizeWidth ) + " " +
    std::to_string( PostScriptPageSizeHeight ) + "\n";
  output += "%%EndComments\n";
  output += "%%BeginSetup\n";
  output += "%%DocumentMedia: ";
  output += PostScriptDocumentMedia;
  output += " ";
  output += std::to_string( PostScriptPageSizeWidth ) + " " +
    std::to_string( PostScriptPageSizeHeight ) + " " +
    std::to_string( PostScriptPageWeight ) + " () ()\n";
  output += "%%EndSetup\n";
  output += "%%Page: 1 1\n";
  output += "%%BeginPageSetup\n";
  output += "save\n";
  output += "% set page size to A4\n";
  output += "<</PageSize [" +
      std::to_string( PostScriptPageSizeWidth ) + " " +
      std::to_string( PostScriptPageSizeHeight ) + " " +
      "] >> setpagedevice\n" +
    "% change scale to millimeters\n" +
    "72 25.4 div dup scale\n" +
    "/pixel { 25.4 " +
      std::to_string( PostScriptDPI ) + " div mul } def\n" +
    "/blbox { newpath moveto 0 setgray 0 setlinewidth\n" +
    "         " +
      std::to_string( xscale ) + " 0 rlineto " +
      "0 " + std::to_string( -yscale ) + " rlineto " +
      std::to_string( -xscale ) + " 0 rlineto\n" +
    "         closepath fill } def\n" +
    "/whbox { newpath moveto 1 setgray 1 setlinewidth\n" +
    "         " +
      std::to_string( xscale ) + " 0 rlineto " +
      "0 " + std::to_string( -yscale ) + " rlineto " +
      std::to_string( -xscale ) + " 0 rlineto\n" +
    "         closepath fill } def\n" +    
    "210 0 translate\n"
    "90 rotate\n"
    "% landscape view with origo in lower left corner\n" +
    "10 10 translate\n" +
    "0 setlinewidth\n";
  output += "%%EndPageSetup\n";
  for( int ix = 0; ix < xlim; ix++ )
  {
    for( int iy = 0; iy < ylim; iy++ )
    {
      output += std::to_string( ix ) + " " +
	std::to_string( xscale ) + " mul " +
	std::to_string( iy ) + " " +
	std::to_string( yscale ) + " mul ";
      if( maze.iswallpoint( ix, iy ) )
	output += " blbox\n";
      else
	output += " whbox\n";
    }
  }
  output += "%%PageTrailer\n";
  output += "\n showpage restore  \n";
  output += "%%Trailer\n";
  return output;
}
