/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include <iostream>
#include <utility>

using namespace std;

#include "mazegrid.hpp"

Mazegrid::Mazegrid( const int x, const int y ) { init( x, y ); };
Mazegrid::Mazegrid( void ) { init( GridXDefaultSize, GridYDefaultSize ); };
std::pair<int,int> Mazegrid::getsize( void ) const { return make_pair( xsize, ysize ); };
int Mazegrid::addwallpoint( const int x, const int y ) { setpoint( x, y, IsWall ); };
int Mazegrid::clearwallpoint( const int x, const int y ) { setpoint( x, y, IsPath ); };

void Mazegrid::init( const int x, const int y ) // private member function
{
#ifdef DEBUG
  fprintf( stderr,
	   "Mazegrid init called with xsize %d and ysize %d\n",
	   x, y );
#endif
  if( !(x & 1) || !(y & 1) || (x < 3) || (y < 3) )
  {
    fprintf( stderr,
	     "Maze grid dimensions must be odd numbers greater than 2\n" );
    exit( 1 );
  }
  xsize = x;
  ysize = y;

  /* Create a new array on the heap. Using dynamic storage
     means that the default copy and assignment operators
     may not work properly. */
  mazepicture = new char[ xsize * ysize ];

  for( int ix = 0; ix < x; ix++ )
    for( int iy = 0; iy < y; iy++ )
      mazepicture[ ix*ysize + iy ] = IsPath;
}

Mazegrid::Mazegrid( const Mazegrid& originalmaze )
{
#ifdef DEBUG
  fprintf( stderr, "Mazegrid copy constructor called\n");
#endif
  pair<int,int> origsize = originalmaze.getsize();
  xsize = origsize.first;
  ysize = origsize.second;
  mazepicture = new char[ xsize * ysize ];
  for( int ix = 0; ix < xsize; ix++ )
    for( int iy = 0; iy < ysize; iy++ )
      mazepicture[ ix*ysize + iy ] =
	originalmaze.mazepicture[ ix*ysize + iy ];
}

Mazegrid& Mazegrid::operator =( const Mazegrid& rightsidemaze )
{
#ifdef DEBUG
  fprintf( stderr, "Mazegrid operator = called\n");
#endif
  pair<int,int> origsize = rightsidemaze.getsize();
  if( ( xsize != origsize.first ) || ( ysize != origsize.second ) )
  {
    delete mazepicture; // No objects inside array, ordinary delete is OK
    xsize = origsize.first;
    ysize = origsize.second;    
    mazepicture = new char[ xsize * ysize ];
  }
  for( int ix = 0; ix < xsize; ix++ )
    for( int iy = 0; iy < ysize; iy++ )
      mazepicture[ ix*ysize + iy ] =
	rightsidemaze.mazepicture[ ix*ysize + iy ];
}

Mazegrid::~Mazegrid( void )
{
  delete mazepicture;  // No objects inside array, ordinary delete is OK
}

int Mazegrid::iswallpoint( const int x, const int y ) const
{
#ifdef DEBUG
    fprintf( stderr,
	     "iswallpoint called with arguments x= %d, y= %d\n",
	     x, y );
#endif
  int retval;
  if( (x < 0) || (x >= xsize) || (y < 0) || (y >= ysize ) )
  {
    return OffGrid;
  }
  return mazepicture[ x*ysize + y ];
}

int Mazegrid::setpoint( const int x, const int y, const GridPointProperties wall )
{
  int retval = Mazegrid::iswallpoint( x, y );
  if( retval == OffGrid ) return retval; // point was off grid
  mazepicture[ x*ysize + y ] = wall;
  return retval;
}

#ifdef LOCALTEST
/* Note: if tests don't work, perhaps you used gcc instead of g++? */
int main()
{
  fprintf( stderr, "Starting tests.\n" );
  int someint;
  pair<int,int> somepair;
  Mazegrid * mazptr;
  Mazegrid * mazecopyptr;
  for( int x = 3; x < 2000; x += 2*x ) // Some valid sizes
    for( int y = 3; y < 2000; y += 2*x )
    {
      mazptr = new Mazegrid(x,y); // Heap alloc
      somepair = mazptr->getsize(); // Did the size work?
      if( (x != somepair.first) || (y != somepair.second) )
	fprintf( stderr,
		 "getsize failed, wanted x= %d, y= %d; got x= %d, y= %d\n",
		 x, somepair.first, y, somepair.second );
      for( int ix = 0; ix < x; ix++ ) // Is grid empty?
	for( int iy = 0; iy < y; iy++ )
	{
	  someint = mazptr->iswallpoint( ix, iy );
	  if( someint != IsPath )
	    fprintf( stderr,
		     "iswallpoint failed at x= %d, y= %d\n: unexpected wall\n",
		     ix, iy );
	}
      for( int ix = -17; ix < 0; ix++ ) // Test outside grid
	for( int iy = -17; iy < 0; iy++ )
	{
	  someint = mazptr->iswallpoint( ix, iy );
	  if( someint != OffGrid )
	    fprintf( stderr,
		     "iswallpoint failed at x= %d, y= %d\n: not -1\n",
		     ix, iy );
	}
      for( int ix = 0; ix < x; ix++ ) // Add some walls
	for( int iy = 0; iy < y; iy++ )
	{
	  someint = mazptr->addwallpoint( ix, iy );
	  if( someint != OffGrid ) someint = mazptr->iswallpoint( ix, iy );
	  if( someint == IsPath ) fprintf( stderr,
				      "addwallpoint failure at x= %d, y= %d\n",
				      ix, iy );
	}
      for( int ix = 0; ix < x; ix++ ) // Now clear the walls
	for( int iy = 0; iy < y; iy++ )
	{
	  someint = mazptr->clearwallpoint( ix, iy );
	  if( someint != OffGrid ) someint = mazptr->iswallpoint( ix, iy );
	  if( someint != IsPath ) fprintf( stderr, 
				      "clearwallpoint failure at x= %d, y= %d\n",
				      ix, iy );
	}
      for( int ix = -17; ix < 0; ix++ ) // Test outside grid
	for( int iy = -17; iy < 0; iy++ )
	{
	  if( mazptr->isedgepoint( ix, iy ) )
	      fprintf( stderr,
		       "isedgepoint failed at x= %d, y= %d\n: not -1\n",
		       ix, iy );  
	  someint = mazptr->addwallpoint( ix, iy );
	  if( someint >= 0 )
	    fprintf( stderr,
		     "addwallpoint failed at x= %d, y= %d\n: not -1\n",
		     ix, iy );
	}
      mazecopyptr = new Mazegrid(*mazptr); // Make copy
      for( int ix = 0; ix < x; ix++ ) // Test that copy has identical contents
	for( int iy = 0; iy < y; iy++ )
	  if( mazptr->iswallpoint( ix, iy ) != mazecopyptr->iswallpoint( ix, iy ) )
	  {
	    fprintf( stderr,
		     "grid copy differs from original at x= %d, y= %d\n\n",
		     ix, iy );
	  }
      for( int ix = 0; ix < x; ix++ ) // Test that original does not change
	for( int iy = 0; iy < y; iy++ ) // when copy is changed
	{
	  mazptr->addwallpoint( ix, iy );
	  mazecopyptr->clearwallpoint( ix, iy );	  
	  if( mazptr->iswallpoint( ix, iy ) == mazecopyptr->iswallpoint( ix, iy ) )
	  {
	    fprintf( stderr,
		     "grid copy not separate from original at x= %d, y= %d\n\n",
		     ix, iy );
	  }
	}
      delete mazecopyptr; // Delete and create default mazegrid
      mazecopyptr = new Mazegrid;
      *mazecopyptr = *mazptr; // Make assignment
      for( int ix = 0; ix < x; ix++ ) // Re-test that copy has identical contents
	for( int iy = 0; iy < y; iy++ )
	  if( mazptr->iswallpoint( ix, iy ) != mazecopyptr->iswallpoint( ix, iy ) )
	  {
	    fprintf( stderr,
		     "grid copy differs from original at x= %d, y= %d\n\n",
		     ix, iy );
	  }
      for( int ix = 0; ix < x; ix++ ) // Test that original does not change
	for( int iy = 0; iy < y; iy++ ) // when copy is changed
	{
	  mazptr->addwallpoint( ix, iy );
	  mazecopyptr->clearwallpoint( ix, iy );	  
	  if( mazptr->iswallpoint( ix, iy ) == mazecopyptr->iswallpoint( ix, iy ) )
	  {
	    fprintf( stderr,
		     "grid copy not separate from original at x= %d, y= %d\n\n",
		     ix, iy );
	  }
	}
      delete mazecopyptr;  // Cleanup
      delete mazptr;
    };
      
  Mazegrid blaze;
  somepair = blaze.getsize(); // Does default size work?
  if( (GridXDefaultSize != somepair.first) || (GridYDefaultSize != somepair.second) )
    fprintf( stderr,
	     "Default size failed, wanted x= %d, y= %d; got x= %d, y= %d\n",
	     GridXDefaultSize, GridYDefaultSize, somepair.first, somepair.second );
  fprintf( stderr, "Finished all tests.\n" );
}
#endif
