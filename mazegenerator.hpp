/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _MAZEGENERATOR_H_
#define _MAZEGENERATOR_H_

#include <random>

#include "mazegrid.hpp"
#include "mazefreelist.hpp"

const int DirectionRight = 1;
const int DirectionUp = 2;
const int DirectionLeft = 4;
const int DirectionDown = 8;

const int DefaultMazeXSize = 3;
const int DefaultMazeYSize = 3;

const int ProbabilityOfChoosingNewWallPoint = 17;
//const int ProbabilityOfChoosingNewDirection = 75;

class Mazegenerator
{
  struct Wallpoint
  {
    int x;
    int y;
  };
 public:
  Mazegenerator( void );
  Mazegenerator( const int x, const int y );
  Mazegenerator( const int x, const int y, const int percentage );
  std::pair<int,int> getsize( void ) const;
  Mazegrid generate( void );
  int iswallpoint( const int x, const int y );
private:
  Mazegrid maze;
  Mazefreelist * wallpoints;
  int maxnumberofwallpoints;
  int currentdirection;
  int currentwallpoint;
  int probability;
  std::random_device randgen;
  Mazegenerator( const Mazegenerator& ); // not copyable
  Mazegenerator& operator =( const Mazegenerator& ); // not assignable
  void init( const int x, const int y, const int percentage );
  void addborder( void );
  void addwall( const int x, const int y );
  int freedirections( const int x, const int y );
  void growwall( const int x, const int y, const int direction );
  void makeexits( void );
  int randomdirection( const int x, const int y );
  bool randompointcheck( void );
  int randompointindex( void );
};

#endif
