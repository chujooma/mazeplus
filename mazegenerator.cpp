/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include <cassert>
#include <iostream>
#include <random>
#include <utility>
#include <vector>

#include "mazefreelist.hpp"
#include "mazegrid.hpp"
#include "mazegenerator.hpp"

using namespace std;

Mazegenerator::Mazegenerator( void )
{
  init( DefaultMazeXSize,
	DefaultMazeYSize,
	ProbabilityOfChoosingNewWallPoint );
};
Mazegenerator::Mazegenerator( const int x, const int y )
{
  init( x, y, ProbabilityOfChoosingNewWallPoint );
};
Mazegenerator::Mazegenerator( const int x,
			      const int y,
			      const int percentage )
{
  init( x, y, percentage );
};

void Mazegenerator::init( const int x, const int y, const int percentage )
{
#ifdef DEBUG
  fprintf( stderr,
	   "Mazegenerator init called. Custom: %s, x: %d, y: %d\n",
	   ( custom ? "true" : "false" ), x, y );
#endif
  maze = Mazegrid( x, y );
  maxnumberofwallpoints =
    ( ( ( maze.getsize().first + 1 ) *
        ( maze.getsize().second + 1 ) ) >> 2 ) + 1;
  wallpoints = new Mazefreelist( maxnumberofwallpoints );
  currentwallpoint = 0;
  currentdirection = 0;
  probability = percentage;
}

void Mazegenerator::addborder( void ) // Adds unbroken border
{
  // Exits must be added later by removing two pieces of wall
  int xlim = maze.getsize().first;
  int ylim = maze.getsize().second;
  for( int ix = 0; ix < maze.getsize().first; ix += 1 )
    addwall( ix, 0 );
  for( int iy = 1; iy < maze.getsize().second; iy += 1 )
    addwall( maze.getsize().first - 1, iy );
  for( int ix = maze.getsize().first - 2; ix >= 0; ix -= 1 )
    addwall( ix, maze.getsize().second - 1 );
  for( int iy = maze.getsize().second - 2; iy > 0; iy -= 1 )
    addwall( 0 , iy );
}

void Mazegenerator::addwall( const int x, const int y )
{
#ifdef DEBUG
  fprintf( stderr,
	   "addwall called with x: %d, y: %d\n", x, y );
#endif
  maze.addwallpoint( x, y );
  if( !(x&1) && !(y&1) ) // On grid
    currentwallpoint = wallpoints->addentry( x, y );
}

int Mazegenerator::freedirections( const int x, const int y )
{
  int retval = 0;
  if( (x | y) & 1 ) return 0;
  retval |= ( maze.iswallpoint( x + 2, y ) ? 0 : DirectionRight );
  retval |= ( maze.iswallpoint( x - 2, y ) ? 0 : DirectionLeft );
  retval |= ( maze.iswallpoint( x, y + 2 ) ? 0 : DirectionUp );
  retval |= ( maze.iswallpoint( x, y - 2 ) ? 0 : DirectionDown );
  return retval;
}

Mazegrid Mazegenerator::generate( void )
{
  addborder();
  /* Always randomize current wall point after drawing border,
     otherwise maze drawing will start at last border point
     (typically, depending on ProbabilityOfChoosingNewWallPoint) */
  currentwallpoint = randompointindex();
  while ( wallpoints->getsize() )
  {
    if( randompointcheck() ) currentwallpoint = randompointindex();
    int x = wallpoints->readentry( currentwallpoint ).first;
    int y = wallpoints->readentry( currentwallpoint ).second;
    currentdirection = randomdirection( x, y );
    if( currentdirection )
    {
      growwall( x, y, currentdirection );
    }
    else
    {
      bool q = wallpoints->rmentry( currentwallpoint );
      if( !q ) { fprintf(stderr, "rmentry from entry queue\n"); exit( 80 ); }
      currentwallpoint = randompointindex();
    }
  }
  makeexits();
  return maze;
}

std::pair<int,int> Mazegenerator::getsize( void ) const
{
  return maze.getsize();
}

void Mazegenerator::growwall( const int x, const int y, const int direction )
{
  switch( direction )
  {
  case DirectionRight:
    addwall( x + 1, y );
    addwall( x + 2, y );
    break;
  case DirectionUp:
    addwall( x, y + 1 );
    addwall( x, y + 2 );
    break;
  case DirectionLeft:
    addwall( x - 1, y );
    addwall( x - 2, y );
    break;
  case DirectionDown:
    addwall( x, y - 1 );
    addwall( x, y - 2 );
    break;
  default:
    fprintf( stderr,
	     "growwall failed due to bad direction: %d\n",
	     direction );
    exit( 73 );
  }
}

int Mazegenerator::iswallpoint( const int x, const int y )
{
  return maze.iswallpoint( x, y );
}

void Mazegenerator::makeexits( void )
{
  int xlim = maze.getsize().first;
  int ylim = maze.getsize().second;
  int epos = (ylim / 2) | 1; // Never on grid!
  maze.clearwallpoint( 0, epos );
  maze.clearwallpoint( xlim - 1, epos );
}

int Mazegenerator::randomdirection( const int x, const int y )
{
  int freedirs = freedirections( x, y );
  int numdirs = 0;
  int dirs[ 4 ];
  if( freedirs & DirectionRight ) dirs[ numdirs++ ] = DirectionRight;
  if( freedirs & DirectionUp ) dirs[ numdirs++ ] = DirectionUp;
  if( freedirs & DirectionLeft ) dirs[ numdirs++ ] = DirectionLeft;
  if( freedirs & DirectionDown ) dirs[ numdirs++ ] = DirectionDown;
  if( !numdirs ) return 0;
  if( numdirs == 1 ) return dirs[ 0 ];
  std::uniform_int_distribution<> distr(0,numdirs-1);
  int selecteddirection = distr(randgen);
  return dirs[ selecteddirection ];
}

bool Mazegenerator::randompointcheck( void )
{
  std::uniform_int_distribution<> percentage_distr(0,100);
  if(percentage_distr( randgen ) <= ProbabilityOfChoosingNewWallPoint)
    return true;
  else
    return false;
}

int Mazegenerator::randompointindex( void )
{
  if( wallpoints->getsize() < 2 ) return 0;
  std::uniform_int_distribution<> distr(0,wallpoints->getsize()-1);
  int foundpoint = distr(randgen);
  return foundpoint;
}
