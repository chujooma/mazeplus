/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <cassert>
#include <utility>

#include "mazefreelist.hpp"

using namespace std;

Mazefreelist::Mazefreelist( const int listsize )
{
#ifdef DEBUG
  fprintf( stderr,
	   "Makefreelist constructor called with size %d\n",
	   listsize );
#endif
  assert( listsize > 0 );
  wallpoints = new struct Wallpoint[ listsize ];
  maxnumberofwallpointsinlist = listsize;
  nextfreewallpoint = 0;
}

Mazefreelist::~Mazefreelist( void )
{
  delete wallpoints;
}

int Mazefreelist::addentry( const int x, const int y )
{
#ifdef DEBUG
  fprintf( stderr,
	   "addentry called with x: %d, y: %d, current size is %d\n",
	   x, y, nextfreewallpoint );
#endif
  if( nextfreewallpoint >= maxnumberofwallpointsinlist )
    return -1;
  int retval = nextfreewallpoint;
  wallpoints[ nextfreewallpoint ].x = x;
  wallpoints[ nextfreewallpoint ].y = y;
  nextfreewallpoint += 1;
  return retval;
}

bool Mazefreelist::rmentry( const int index )
{
#ifdef DEBUG
  fprintf( stderr,
	   "rmentry called with index %d, current size is %d\n",
	   index, nextfreewallpoint );
#endif
  if( index >= nextfreewallpoint ) // Index too big
    return false;
  if( !nextfreewallpoint ) // List empty
    return false;
  nextfreewallpoint -= 1;
  wallpoints[ index ].x = wallpoints[ nextfreewallpoint ].x;
  wallpoints[ index ].y = wallpoints[ nextfreewallpoint ].y;
  return true;
}

std::pair<int,int> Mazefreelist::readentry( const int index )
{
  if( index >= nextfreewallpoint ) // Index too big
    return make_pair( -1, -1 );
  if( !nextfreewallpoint ) // List empty
    return make_pair( -1, -1 );
  return make_pair( wallpoints[ index ].x,
			    wallpoints[ index ].y );
}

int Mazefreelist::getsize( void )
{
  return nextfreewallpoint ;
}

int Mazefreelist::getmaxsize( void )
{
  return maxnumberofwallpointsinlist;
}

#ifdef LOCALTEST
/* Note: if tests don't work, perhaps you used gcc instead of g++? */
int main()
{
  fprintf( stderr, "Starting tests of mazefreelist.\n" );
  int someint;
  pair<int,int> somepair;
  Mazefreelist * tomat;
  for( int in = 1; in < 32769; in = in + in )
  {
    tomat = new Mazefreelist( in );
    int g = tomat->getmaxsize();
    if( g != in )
      fprintf( stderr,
	       "Test failed, size should be %d but is %d\n",
	       in, g );
    for( int in = 0; in < tomat->getmaxsize(); in += 1 )
    {
      int tmp = tomat->addentry( 4711, in );
      if( tmp < 0 )
	fprintf( stderr,
		 "Test failed, couldn't add entry, size is %d\n",
		 tomat->getsize() );
      somepair = tomat->readentry( in );
      if( somepair != make_pair( 4711, in ) )
	fprintf( stderr,
		 "Test failed, couldn't read entry %d\n",
		 in );
    }
    for( int in = 0; in < tomat->getmaxsize(); in += 1 )
      if( !(tomat->rmentry( 0 )) )
	fprintf( stderr,
		 "Test failed, couldn't remove entry number %d\n",
		 in );
    delete tomat;
  }
  fprintf( stderr, "Finished tests of mazefreelist.\n" );
}
#endif
