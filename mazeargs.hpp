/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _MAZEARGS_HPP_
#define _MAZEARGS_HPP_

#include <getopt.h> // Required for getopt_long
#include <iostream>

enum MazeOutputTypes { OutputInvalid = -1, OutputPostScript = 1 };

const int MazeXDefaultSize = 55;
const int MazeYDefaultSize = 39;
const int MazeDefaultProbability = 17;
const int MazeDefaultOutputType = OutputPostScript;

class Mazeargumentprocessor
{
public:
  void doargs(int argc, char** argv);
  int getxsize( void );
  int getysize( void );
  int getpercentage( void );
private:
  void printhelp( std::string progname );
  void printinfo( std::string progname );
  void printversion( void );
  int xsize = MazeXDefaultSize;
  int ysize = MazeYDefaultSize;
  int perc = MazeDefaultProbability;
};
#endif
