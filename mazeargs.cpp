/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h> // Required for getopt_long
#include <iostream>

#include "main.hpp"
#include "mazeargs.hpp"

int num = -1;
bool is_beep = false;
float sigma = 2.034;
std::string write_file = "default_file.txt";

void Mazeargumentprocessor::doargs(int argc, char** argv)
{
  const char* const short_opts = "x:y:p:hv";
  const option long_opts[] =
  {
    {"xsize", required_argument, nullptr, 'x'},
    {"ysize", required_argument, nullptr, 'y'},
    {"percentage", required_argument, nullptr, 'p'},
    {"help", no_argument, nullptr, 'h'},
    {"version", no_argument, nullptr, 'v'},
    {nullptr, no_argument, nullptr, 0}
  };

  while (true)
  {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);
    if ( opt < 0 ) break;
    switch (opt)
    {
    case 'x':
      xsize = std::stoi(optarg);
      break;
    case 'y':
      ysize = std::stoi(optarg);
      break;
    case 'p':
      perc = std::stoi(optarg);
      break;
    case 'v':
      printversion();
      break;
    case '?': // Unrecognized option
      printinfo( argv[ 0 ] );
      break;
    case 'h': // -h or --help
    default:
      printhelp( argv[ 0 ] );
      break;
    }
  }
}

int Mazeargumentprocessor::getxsize( void )
{
  return( xsize );
}

int Mazeargumentprocessor::getysize( void )
{
  return( ysize );
}

int Mazeargumentprocessor::getpercentage( void )
{
  return( perc );
}

void Mazeargumentprocessor::printhelp( std::string progname )
{
  std::cout <<
    "typical usage: " << progname << " | ps2pdf - > tmp.pdf\n"
    "-x num, --xsize num\tSet width of maze\n"
    "-y num, --ysize num\tSet height of maze\n"
    "Width and height must be odd numbers and greater than 2\n"
    "\n"
    "-p num, --percentage num\tSet maze generation parameter\n"
    "Percentage must at least 0 and at most 100\n"
    "\n"
    "-v, --version\tPrint version info\n";
    "-h, --help\tPrint this text\n";
  exit(1);
}

void Mazeargumentprocessor::printinfo( std::string progname )
{
  std::cout <<
    "typical usage: " << progname << " | ps2pdf - > tmp.pdf\n"
    "For more help, use flag -h or --help\n";
  exit(1);
}

void Mazeargumentprocessor::printversion( void )
{
  std::cout << progversion << "\n";
  exit(1);
}

#ifdef LOCALTEST
int main(int argc, char * argv[] )
{
  Mazeargumentprocessor arg;
  arg.doargs(argc, argv);
  fprintf( stderr, "x= %d, y= %d, p= %d\n",
	   arg.getxsize(), arg.getysize(), arg.getpercentage() );
  return 0;
}
#endif
