/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "main.hpp"
#include "mazeargs.hpp"
#include "mazegenerator.hpp"
#include "mazepsprint.hpp"

Mazegrid maze;

int main(int argc, char * argv[] )
{
  Mazeargumentprocessor arg;
  arg.doargs(argc, argv);
#ifdef DEBUG
  fprintf( stderr, "x= %d, y= %d, p= %d\n",
	   arg.getxsize(), arg.getysize(), arg.getpercentage() );
#endif
  Mazegenerator newmaze( arg.getxsize(), arg.getysize(), arg.getpercentage() );
  maze = newmaze.generate();
#ifdef DEBUG
  fprintf( stderr, "Maze generated now\n" );
#endif
  MazePSconverter cvtps;
  std::cout << cvtps.maze2string( maze );
}
