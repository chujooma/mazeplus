/*
  Copyright 2018, F Lundevall.

  This file is part of Mazeplus.

  Mazeplus is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Mazeplus is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Mazeplus.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _MAZEFREELIST_H_
#define _MAZEFREELIST_H_

#include <utility>

class Mazefreelist
{
  struct Wallpoint
  {
    int x;
    int y;
  };
public:
  Mazefreelist( const int listsize );
  ~Mazefreelist( void );
  int addentry( const int x, const int y );
  bool rmentry( const int index );
  std::pair<int,int> readentry( const int index );
  int getsize( void );
  int getmaxsize( void );
private:
  struct Wallpoint * wallpoints;
  int maxnumberofwallpointsinlist;
  int nextfreewallpoint;
  Mazefreelist( void ); // Not constructable without a size
  Mazefreelist( const Mazefreelist& ); // not copyable
  Mazefreelist& operator =( const Mazefreelist& ); // not assignable
};
#endif
